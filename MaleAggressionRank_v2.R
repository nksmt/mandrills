library("tidyverse")
library("stats")
library("lme4")
library("glmmTMB")
library("stargazer")
library("texreg")
library("xtable")
library("tikzDevice")
library("datasets")
library("ggsignif")
library("blme")
library("car")
library("effects")
library("kableExtra")
library("DHARMa")


###################### Male Rank and Aggression #########################
# Are alpha males more aggressive towards females than non-alpha males?
STATagRankAM <- read.csv("AggressionRankTable.csv")

############### TABLE LEGEND ###############
# id: The id of the male 
# Month: Month of observation
# Year: Year of observation
# AggToAFs: Male aggression events towards any adult female
# AMnAFsObsTime: The total time of observation of the male and all the females during this month
# swolSex: Sex ratio including swollen females (not only maxially swollen females like the operational sex ratio)
# rankAM: Is the male alpha?
# age: The age of the male


modAgRank <- glmmTMB(AggToAFs ~ 
                       rankAM +
                       age +
                       swolSex +
                       (1 | id) + (1 | Year) +
                       offset(log(AMnAFsObsTime))  # fixed effect instead of offset since it's binomial ???
                     , family = nbinom2
                     , data = STATagRankAM)


car::Anova(modAgRank)
plot(allEffects(modAgRank))
summary(modAgRank)
testDispersion(modAgRank)
simulateResiduals(fittedModel = modAgRank, plot = T)



##################################### PLOT ############################################

STATagRankAMana <- STATagRankAM %>% 
  mutate(rankAM=replace(rankAM, rankAM== TRUE, "Alpha"),
         rankAM=replace(rankAM, rankAM== FALSE, "Non-alpha")) 

  ggplot(STATagRankAMana, aes(rankAM, fitted(modAgRank))) +
  geom_violin(width=1, col="black", fill = "#999999") +
  geom_boxplot(outlier.shape=NA, width=0.15, color="black", alpha=0.2, notch = FALSE) +
  geom_signif(comparisons = list(c("Alpha", "Non-alpha")), textsize=5, vjust = 0.5, annotation = c("*"), color="black",
              map_signif_level = TRUE, y_position = 4) +
  theme_classic() +
  ylab("Predicted rate of aggression of a male \n towards adult females") +
  xlab("Male rank") +
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 22, color="black"),
        axis.line.y = element_line(color = "black"),
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "white"),
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), # , face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=20, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2))
  