library("tidyverse")
library("lubridate")
library("stats")
library("lme4")
library("stargazer")
library("texreg")
library("xtable")
library("tikzDevice")
library("datasets")
library("ggsignif")
library("blme")
library("effects")
library("DHARMa")

#################### Injuries and Reproductive state #########################
# Are swollen females getting injured more often?
STATinjCyF <- read.csv("2ndPredictionTable.csv")

############### TABLE LEGEND ###############
# id: The id of the female
# date_recens: Date of observation
# year: Year of observation
# month: Month of observations
# cycState: The reproductive state of the female
# groupSex: The group sex ratio
# operSex: The operational sex ration
# rank: The rank of the female (LR, MR, HR: Low, Medium, High Rank)
# parous: Was the female parous this day?


# Specify reference category by changing the order of the levels 
STATinjCyF$cycState <- factor(STATinjCyF$cycState, levels = c("Non-Swollen", "Swollen", "Pregnant", "Lactating")) # ns
# STATinjCyF$cycState <- factor(STATinjCyF$cycState, levels = c("Swollen", "Non-Swollen", "Pregnant",  "Lactating")) # s
# STATinjCyF$cycState <- factor(STATinjCyF$cycState, levels = c("Pregnant", "Non-Swollen", "Swollen",  "Lactating")) # g
# STATinjCyF$cycState <- factor(STATinjCyF$cycState, levels = c("Lactating", "Non-Swollen", "Swollen", "Pregnant")) # l

# STATinjCyF$rank <- factor(STATinjCyF$rank, levels = c("HR", "MR", "LR"))

# Scale group sex ratio
STATinjCyF$groupSex <- scale(STATinjCyF$groupSex)

modInjRepStt <- glmer(injured ~ 
                        cycState + rank + parous + groupSex + (1 | id) + (1 | year),
                   family = binomial, data = STATinjCyF,
                   control=glmerControl(optimizer="bobyqa",
                                        optCtrl=list(maxfun=2e5)))


car::Anova(modInjRepStt)
plot(allEffects(modInjRepStt))
summary(modInjRepStt)
# testDispersion(modInjRepStt)
# simulationOutput <- simulateResiduals(fittedModel = modInjRepStt, plot = T)



#################################### PLOT ############################################

injBinState <- STATinjCyF[!is.na(STATinjCyF$rank),]
injBinState <- injBinState[!is.na(injBinState$cycState),]

ggplot(injBinState, aes(cycState, fitted(modInjRepStt))) + 
  geom_violin(width=1, col="black", fill = "#999999") +
  geom_boxplot(outlier.shape=NA, width=0.15, color="black", alpha=0.2, notch = FALSE) +
  # ylim(0, 0.013) +  
  geom_signif(comparisons = list(c("Non-Swollen", "Swollen")), vjust = 0.6, textsize=5, annotation = c("***"), color="black", 
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.0153) + 
  geom_signif(comparisons = list(c("Swollen", "Pregnant")), vjust = 0.6, textsize=5, annotation = c("***"), color="black",
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.0153) +
  geom_signif(comparisons = list(c("Pregnant", "Lactating")), vjust = -0, textsize=5, annotation = c("ns"), color="black",
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.0153) +
  geom_signif(comparisons = list(c("Non-Swollen", "Pregnant")), vjust = -0, textsize=5, annotation = c("ns"), color="black", hjust = 1.5,
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.016) +
  geom_signif(comparisons = list(c("Swollen", "Lactating")), vjust = 0.6, textsize=5, annotation = c("***"), color="black",
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.0167) +
  geom_signif(comparisons = list(c("Non-Swollen", "Lactating")), vjust = -0, textsize=5, annotation = c("ns"), color="black", # Here is "." normally
              map_signif_level = TRUE, tip_length = 0.02, y_position = 0.0174) +
  theme_classic() +
  xlab("Reproductive state") +
  ylab("Predicted probability of injury") +
  theme(panel.background = element_rect(fill = "transparent"),
        plot.background = element_rect(fill = "transparent", color = NA),
        panel.grid.major = element_blank(), # get rid of major grid
        panel.grid.minor = element_blank(), # get rid of minor grid
        legend.background = element_rect(fill = "transparent"), # get rid of legend bg
        legend.box.background = element_rect(fill = "transparent"), # get rid of legend panel bg
        axis.title = element_text(size = 22, color="black"),
        axis.line.y = element_line(color = "black"), 
        axis.ticks.y = element_line(color = "black"),
        axis.text.y = element_text(color = "black", size=15),
        axis.line.x = element_line(color = "white"), 
        axis.ticks.x = element_line(color = "black"),
        axis.text.x = element_text(color = "black", size=17), #, face="bold"
        axis.title.x = element_text(vjust = -0.3),
        plot.title=element_text(size=20, 
                                face="bold", 
                                family="American Typewriter",
                                color="black",
                                hjust=0.5,
                                lineheight=1.2))




